
<!-- README.md is generated from README.Rmd. Please edit that file -->

# yoannraffeneln3

yoannraffeneln3 est un package test pour vérifier mon niveau pour la
formation Shiny

## Installation

Vous pouvez installer le package à partir du .tar.gz du package

``` r
library(yoannraffeneln3)
dire_bonjour()
#> Bonjour, toi
```
